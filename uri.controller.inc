<?php
/**
 * @file
 */
/**
 * Provides methods for managing uri's.
 */
class UriController extends DrupalDefaultEntityController {

  public $status;
  
  public function __construct($entityType) {
    $this->idKey = 'uid';
  }
  /**
   * Creates the initial object.
   * @param type $type
   * @return type
   */
  public function create($type = '') {
    return (object) array(
      'uid' => '',
      'type' => $type,
      'title' => '',
    );
  }

  /**
   * Saves the Uri to the database.
   * @param type $uri
   * @return boolean
   */
  public function save($uri) {
    $transaction = db_transaction();
    try {
      $uri->is_new = empty($uri->uid);
      if (empty($uri->created)) {
        $uri->created = REQUEST_TIME;
      }
      $uri->parts = parse_url($uri->uri);
      dpm($uri);
      $did = db_select("uri_domain", "d")
        ->fields("d", array("did"))
        ->condition("name", $uri->parts['host'])
        ->execute()
        ->fetchField();
      if (empty($did)) {
        $did = db_insert('uri_domain')
          ->fields(array(
            'name' => $uri->parts['host'],
          ))->execute();
      }
      $combined_parts = isset($uri->parts['port'])?":".$uri->parts['port']:"";
      $combined_parts .= isset($uri->parts['path'])?$uri->parts['path']:"";
      $combined_parts .= isset($uri->parts['query'])?"?" . $uri->parts['query']:"";
      $combined_parts .= isset($uri->parts['fragment'])?"#" . $uri->parts['fragment']:"";
      $uid = db_insert('uri_part')
        ->fields(array(
          'scheme' => $uri->parts['scheme'],
          'parts' => $combined_parts,
          'did' => $did,
          ))->execute();
      return $uid;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('uri', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }
  
  public function load($ids = array(), $conditions = array()) {
    //dpm($ids);
    //dpm($conditions);
    $entities = array();
    $queried_entities = array();
    
    $passed_ids = !empty($ids) ? array_flip($ids) : FALSE;
    
    if ($ids === FALSE || $ids  || ($conditions && !$passed_ids)) {
      // Build the query.
      $query = $this->buildQuery($ids, $conditions);
      $queried_entities = $query->execute()->fetchAllAssoc($this->idKey);
    }

    if ($passed_ids) {
      // Remove any invalid ids from the array.
      $passed_ids = array_intersect_key($passed_ids, $entities);
      foreach ($entities as $entity) {
        $passed_ids[$entity->{$this->idKey}] = $entity;
      }
      $entities = $passed_ids;
    }
  
    //dpm($queried_entities);
    if ($ids) {
      foreach ($ids as $id) {
        $queried_entities[$id]->uri = $queried_entities[$id]->scheme . '://' .
           $queried_entities[$id]->name . $queried_entities[$id]->parts;

        unset($queried_entities[$id]->scheme);
        unset($queried_entities[$id]->name);
        unset($queried_entities[$id]->parts);
      }
    }
    return $queried_entities;
  }
  
  protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
    //dpm($ids);
    $query = db_select('uri_part', 'uri_part');
    // SELECT CONCAT(p.scheme,'://',d.name,p.parts) 
    //   FROM `uri_part` p INNER JOIN uri_domain d ON p.did=d.did
    $query->join('uri_domain', 'd', 'uri_part.did = d.did');
    $query->fields('uri_part', array('uid', 'scheme', 'parts'))
      ->fields('d', array('name'));
    if ($ids) {
      $query->condition("uri_part.{$this->idKey}", $ids, 'IN');
    }
    return $query;
  }
  
}

/**
 * Represents a URI which is stored in the default Drupal database.
 */
class UriEntity extends Entity {

  /**
   * @return array
   */
  protected function defaultUri() {
    return array('path' => 'uri/' . $this->identifier());
  }
}

/**
 * A while type entity.
 */
class UriType extends Entity {

  public $label = '';
  public $weight = 0;
  public $uri;

  /**
   * Helper method to check if the while type supports revisions.
   *
   * @return bool
   *   TRUE if revision support is enabled, FALSE if not.
   */
  public function supportsRevisions() {
    return !empty($this->data['supports_revisions']);
  }

}
/**
 * Uri Views Controller class.
 */
class UriViewsController extends EntityDefaultViewsController {

  /**
   * Edit or add extra fields to views_data().
   */
  public function views_data() {
    $data = parent::views_data();

    // Add your custom data here

    /*
    * Example: change the handler of a field
    * if the 'created' field is a unix timestamp in the database,
    * Entity API will set the handler to views_handler_field_numeric,
    * change this to the views date handler
    */
    $data['uri']['created']['field']['handler'] = 'views_handler_field_date';

    return $data;
  }
}