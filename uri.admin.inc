<?php

/**
 * @file
 * White label entity type editing UI.
 */

/**
 * UI controller.
 */
class UriTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = uri_get_string('admin menu description');
    return $items;
  }
}

/**
 * Generates the while type editing form.
 */
function uri_type_form($form, &$form_state, Entity $uri_type, $op = 'edit') {

  if ($op == 'clone') {
    $uri_type->label .= ' (cloned)';
    $uri_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $uri_type->label,
    '#description' => t('The human-readable name of this @while-type.', 
      array('@while-type' => uri_get_string('type label'))),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($uri_type->name) ? $uri_type->name : '',
    '#maxlength' => 32,
    '#disabled' => $uri_type->hasStatus(ENTITY_IN_CODE) && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'uri_type_load',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this @while-type. It must only contain lowercase letters, numbers, and underscores.', array('@while-type' => uri_get_string('type label'))),
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $uri_type->weight,
    '#description' => t('When showing @whiles, those with lighter (smaller) weights get listed before @whiles with heavier (larger) weights.', array('@whiles' => uri_get_string('entity plural label'))),
    '#weight' => 10,
  );

  $form['data']['#tree'] = TRUE;
  $form['data']['supports_revisions'] = array('#type' => 'checkbox',
    '#title' => t('Support revisions'),
    '#default_value' => !empty($uri_type->data['supports_revisions']),
    '#description' => t('Enable revision support for this @while-type.', array('@while-type' => uri_get_string('type label'))),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save @while-type', array('@while-type' => uri_get_string('type label'))),
    '#weight' => 40,
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete @while-type', array('@while-type' => uri_get_string('type label'))),
    '#weight' => 45,
    '#limit_validation_errors' => array(),
    '#submit' => array('uri_type_form_submit_delete'),
    '#access' => !$uri_type->hasStatus(ENTITY_IN_CODE) && $op != 'add' && $op != 'clone'
  );
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function uri_type_form_submit(&$form, &$form_state) {
  $uri_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $uri_type->save();
  $form_state['redirect'] = uri_get_string('admin menu path');
}

/**
 * Form API submit callback for the delete button.
 */
function uri_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = uri_get_string('admin menu path') . '/' . $form_state['uri_type']->name . '/delete';
}